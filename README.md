# Moyix Gitlab Task by Craig Ferguson

## Overview

This Python library project was developed to set up a client and server for processing model data. The model data can be downloaded from https://huggingface.co/moyix.

## Prerequisites

Please ensure you have the following prerequisites installed:

- Docker Compose version 1.28 or higher

## Usage

In order to run the code, you need to use the following command:

```bash
docker-compose up --build
```

Then, you can send a curl request like so to make multiple simultaneous requests:

```bash
for i in {1..3}
do
   curl -X POST "http://127.0.0.1:9000/completion/" -H "accept: application/json" -H "Content-Type: application/json" -d "{\"prompt\":\"def hello\"}" &
done
wait
```
Or using [Perf_analyzer](https://github.com/triton-inference-server/client/blob/main/src/c%2B%2B/perf_analyzer/README.md)

```bash
export RELEASE=22.09
docker run --gpus all --rm -it --net gitlab_default nvcr.io/nvidia/tritonserver:${RELEASE}-py3-sdk
```
Then inside the running container:
```bash
perf_analyzer -m ensemble --percentile=95 --concurrency-range 1:4 --input-data test.json -u triton:8000
```
Results such as:
```bash
Inferences/Second vs. Client p95 Batch Latency
Concurrency: 1, throughput: 11.1652 infer/sec, latency 89330 usec
Concurrency: 2, throughput: 11.4431 infer/sec, latency 175948 usec
Concurrency: 3, throughput: 11.385 infer/sec, latency 263871 usec
Concurrency: 4, throughput: 11.3296 infer/sec, latency 352461 usec
```

## Batching for Performance Improvement

To further enhance the performance of this project, we can leverage the concept of dynamic batching. Dynamic batching is a technique where incoming requests are batched together and processed as a single request.

In the case of this project, we can could tokenize multiple requests passed into the python backend with dynamic batching enabled. Then with a supported tokenizer, tokenize in a stacked fashion. The Python backend requires each request to have a response, so we can pass them on through the ensemble in separate response objects. Once processed through fastertransformer backend, the batched request can be split up in the decoder and returned as individual Triton responses. This process can help to significantly reduce the processing time and improve the overall performance by leveraging batching in Triton.

## Inspiration & Known Issues

This solution was inspired by the [fauxpilot](https://github.com/fauxpilot/fauxpilot) repository.

During the development of this project, I encountered difficulty with the completions. Despite the fact that the inputs and shapes into the fastertransformer model are identical, the model is returning different results. Unfortunately, due to time constraints, the root cause of this issue could not be identified and resolved. I would appreciate contributions from the community to help troubleshoot and rectify this issue.


## DEBUGGING CURRENT ISSUE

My Fastertransformer codegen-350M-mono model inputs
```bash
INPUT_IDS input shape (1, 2)
 [[ 4299 23748]]
REQUEST_INPUT_LEN input shape (1, 1)
 [[2]]
REQUEST_OUTPUT_LEN input shape (1, 1)
 [[16]]
runtime_top_k input shape (1, 1)
 [[0]]
runtime_top_p input shape (1, 1)
 [[1.]]
beam_search_diversity_rate input shape (1, 1)
 [[0.]]
random_seed input shape (1, 1)
 [[1314046933]]
temperature input shape (1, 1)
 [[0.2]]
len_penalty input shape (1, 1)
 [[1.]]
model_repetition_penalty input shape (1, 1)
 [[1.]]
is_return_log_probs input shape (1, 1)
 [[ False ]]
beam_width input shape (1, 1)
 [[1]]
start_id input shape (1, 1)
 [[50256]]
end_id input shape (1, 1)
 [[50256]]
bad_words_list input shape (1, 2, 1)
 [[[ 0]
  [-1]]]
stop_words_list input shape (1, 2, 1)
 [[[ 0]
  [-1]]]

```
Receives outputs
```bash
[[ 4299 23748 41527 44665  6900 11698 11698 30592 30592   313 49107 49107 20350 17339 42128  5305 16243  6742]]]
```
Moyix Fastertransformer codegen-350M-mono Model
```bash
input_ids input shape (1, 2)
 [[ 4299 23748]]
input_lengths input shape (1, 1)
 [[2]]
request_output_len input shape (1, 1)
 [[16]]
runtime_top_k input shape (1, 1)
 [[0]]
runtime_top_p input shape (1, 1)
 [[1.]]
beam_search_diversity_rate input shape (1, 1)
 [[0.]]
random_seed input shape (1, 1)
 [[1314046933]]
temperature input shape (1, 1)
 [[0.6]]
len_penalty input shape (1, 1)
 [[1.]]
repetition_penalty input shape (1, 1)
 [[1.]]
is_return_log_probs input shape (1, 1)
 [[False]]
beam_width input shape (1, 1)
 [[1]]
start_id input shape (1, 1)
 [[50256]]
end_id input shape (1, 1)
 [[50256]]
bad_words_list input shape (1, 2, 1)
 [[[ 0]
  [-1]]]
stop_words_list input shape (1, 2, 1)
 [[[ 0]
  [-1]]]
```

Receives outputs for the same seed

```bash
output data [[[ 4299 23748   62  6894  7  944  2599   198 50280  7783   705 15496    11  2159 13679   628 50284   2]]]
```