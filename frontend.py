from fastapi import FastAPI
from functools import lru_cache
from pydantic import BaseModel
from tritonclient.http import (
    InferenceServerClient,
    InferInput,
    InferRequestedOutput,
)
from tritonclient.utils import np_to_triton_dtype
import numpy as np

app = FastAPI()


class APIRequest(BaseModel):
    prompt: str


class TokensExceedsMaximum(Exception):
    pass


TRITON_HOST = "triton"
TRITON_PORT = "8000"
MAX_MODEL_LEN = 2048
PAD_CHAR = 50256

@lru_cache(maxsize=None) 
def get_triton_client():
    ''' create a Triton client '''
    triton_client = InferenceServerClient(url=f"{TRITON_HOST}:{TRITON_PORT}", verbose=False)
    return triton_client

def prepare_tensor(name: str, tensor_input):
    ''' a util to help create Triton Inputs, set data, and types '''
    t = InferInput(name, tensor_input.shape, np_to_triton_dtype(tensor_input.dtype))
    t.set_data_from_numpy(tensor_input)
    return t

def manage_api_request(api_request):
    '''
    This logic is largely borrowed from https://github.com/fauxpilot/fauxpilot/blob/main/copilot_proxy/utils/codegen.py
    The encoding and decoding has been moved to Triton Python Backends however, in order support a more scalable implementation.
    (fast api workers can be scaled as needed. Also batches are built in the encoder and decoder)
    '''
    MODEL_NAME = "ensemble"
    triton_client = get_triton_client()
    prompt = np.array([[api_request.prompt.encode()]]).astype(bytes)
    input_start_ids = np.array([len(api_request.prompt.split())])
    input_len = np.array([[len(api_request.prompt.split())]])
    data = {"n": 1}
    max_tokens = data.get("max_tokens", 16)
    prompt_tokens = input_len[0][0]
    requested_tokens = max_tokens + prompt_tokens
    if requested_tokens > MAX_MODEL_LEN:
        raise TokensExceedsMaximum(
            f"This model's maximum context length is {MAX_MODEL_LEN}, however you requested "
            f"{requested_tokens} tokens ({prompt_tokens} in your prompt; {max_tokens} for the completion). "
            f"Please reduce your prompt; or completion length."
        )
    output_len = np.ones_like(input_len).astype(np.uint32) * max_tokens
    num_logprobs = data.get("logprobs", -1)
    if num_logprobs is None:
        num_logprobs = -1
    want_logprobs = False

    temperature = data.get("temperature", 0.6)
    if temperature == 0.0:
        temperature = 1.0
        top_k = 1
    else:
        top_k = data.get("top_k", 0)

    top_p = data.get("top_p", 1.0)
    frequency_penalty = data.get("frequency_penalty", 1.0)
    runtime_top_k = top_k * np.ones([input_start_ids.shape[0], 1]).astype(np.uint32)
    runtime_top_p = top_p * np.ones([input_start_ids.shape[0], 1]).astype(np.float32)
    beam_search_diversity_rate = 0.0 * np.ones([input_start_ids.shape[0], 1]).astype(
        np.float32
    )
    random_seed = np.random.randint(
        0, 2**31 - 1, (input_start_ids.shape[0], 1), dtype=np.int32
    )
    random_seed = np.array([[1314046933]], dtype=np.int32)
    temperature = temperature * np.ones([input_start_ids.shape[0], 1]).astype(
        np.float32
    )
    len_penalty = 1.0 * np.ones([input_start_ids.shape[0], 1]).astype(np.float32)
    repetition_penalty = frequency_penalty * np.ones(
        [input_start_ids.shape[0], 1]
    ).astype(np.float32)
    is_return_log_probs = want_logprobs * np.ones([input_start_ids.shape[0], 1]).astype(
        np.bool_
    )
    beam_width = (1 * np.ones([input_start_ids.shape[0], 1])).astype(np.uint32)
    start_ids = PAD_CHAR * np.ones([input_start_ids.shape[0], 1]).astype(np.uint32)
    end_ids = PAD_CHAR * np.ones([input_start_ids.shape[0], 1]).astype(np.uint32)

    stop_word_list = np.concatenate(
        [
            np.zeros([input_start_ids.shape[0], 1, 1]).astype(np.int32),
            (-1 * np.ones([input_start_ids.shape[0], 1, 1])).astype(np.int32),
        ],
        axis=1,
    )

    bad_words_list = np.concatenate(
        [
            np.zeros([input_start_ids.shape[0], 1, 1]).astype(np.int32),
            (-1 * np.ones([input_start_ids.shape[0], 1, 1])).astype(np.int32),
        ],
        axis=1,
    )

    # Create Inputs to the ensemble
    inputs = [
        prepare_tensor("prompt", prompt),
        prepare_tensor("request_output_len", output_len),
        prepare_tensor("temperature", temperature),
        prepare_tensor("model_repetition_penalty", repetition_penalty),
        prepare_tensor("runtime_top_k", runtime_top_k),
        prepare_tensor("runtime_top_p", runtime_top_p),
        prepare_tensor("len_penalty", len_penalty),
        prepare_tensor("beam_width", beam_width),
        prepare_tensor("beam_search_diversity_rate", beam_search_diversity_rate),
        prepare_tensor("start_id", start_ids),
        prepare_tensor("end_id", end_ids),
        prepare_tensor("random_seed", random_seed),
        prepare_tensor("is_return_log_probs", is_return_log_probs),
        prepare_tensor("bad_words_list", bad_words_list),
        prepare_tensor("stop_words_list", stop_word_list),
    ]

    # Create Outputs
    outputs = [
        InferRequestedOutput("completions"),
        InferRequestedOutput("output_ids"),
        InferRequestedOutput("sequence_length"),
    ]

    # Run inference
    results = triton_client.infer(model_name=MODEL_NAME, inputs=inputs, outputs=outputs)

    # Extract results
    result = results.as_numpy("completions")
    return result[0].decode()


@app.post("/completion/")
async def get_model_inference(api_request: APIRequest):
    result = manage_api_request(api_request)
    return {"completion": result}
