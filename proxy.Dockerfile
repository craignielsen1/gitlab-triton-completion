FROM python:3.10-slim-buster

WORKDIR /python-docker

COPY pyproject.toml pyproject.toml

RUN pip install poetry uvicorn
RUN poetry install

COPY frontend.py .

EXPOSE 9000

CMD ["poetry", "run", "uvicorn", "--host", "0.0.0.0", "--port", "9000", "frontend:app", "--workers", "4"]
