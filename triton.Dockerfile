FROM moyix/triton_with_ft:22.09

# Install dependencies: torch
RUN python3 -m pip install --disable-pip-version-check -U torch --extra-index-url https://download.pytorch.org/whl/cu116
RUN python3 -m pip install --disable-pip-version-check -U transformers bitsandbytes accelerate

ARG MODEL_VERSION="1"
ARG MODEL_REPO="model_repository"
ARG MODELS_ROOT="/model"
ARG TEMPNAME="archive.tar.zst"

ENV MODEL="codegen-350M-mono"

RUN apt-get update
RUN apt-get -y install \
    zstd \
    curl

# For now, I am storing the model configuration directly on the image
# this should be stored on cloud blob storage in the future
# and dynamically loaded into triton as required
WORKDIR "/temp"

# fetch the moyix smallest model
RUN curl -L "https://huggingface.co/moyix/${MODEL}-gptj/resolve/main/${MODEL}-1gpu.tar.zst"  -o "$TEMPNAME"

RUN zstd -dc "$TEMPNAME" | tar -xf - -C "."

RUN ls  /temp/codegen-350M-mono-1gpu/fastertransformer/1
WORKDIR "${MODELS_ROOT}"

COPY model_repository/ensemble ensemble
RUN mkdir -p ensemble/${MODEL_VERSION} && cat /dev/null > ensemble/${MODEL_VERSION}/.tmp

COPY ${MODEL_REPO}/fastertransformer fastertransformer
RUN mv /temp/codegen-350M-mono-1gpu/fastertransformer/1 fastertransformer
RUN ls fastertransformer/1

COPY ${MODEL_REPO}/postprocessing postprocessing
COPY ${MODEL_REPO}/preprocessing preprocessing

# copy the moyix token
COPY ${MODEL_REPO}/cgtok preprocessing/1/cgtok
COPY ${MODEL_REPO}/cgtok postprocessing/1/cgtok

RUN apt-get install tree
RUN tree /model
RUN tree /model
